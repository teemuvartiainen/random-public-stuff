/*
 * TEST FILE AND ITERATION EXAMPLE
 */
#include <stdio.h>
#include <stdlib.h>
#include "ll_linkedlist.h"

// asd
// this is an example function to be given to the iterate function as a parameter
// this will be called separately for each node, in order
// remember to return a nonzero int!
int traverse_example(void *n) {
	// remember this typecast at the start!
	ll_node *node = ((ll_node*)n);
	
	// let's just print something to test
	if (!node->connected) {
		printf("%d,%d disconnected\n", node->xvalue, node->yvalue);
	} else {
		printf("%d,%d connected!\n", node->xvalue, node->yvalue);
	}

  return 1;
}

int main(void) {

	// initialize a pointer
	ll_list *list = NULL;
	
	// create a new list there
	linkedlist_begin(&list);

	// append some crap
	linkedlist_append(&list, 1, 2, 0);
	linkedlist_append(&list, 3, 4, 1);
	linkedlist_append(&list, 5, 6, 1);

	linkedlist_append(&list, 6, 9, 0);
	linkedlist_append(&list, 7, 9, 1);
	linkedlist_append(&list, 8, 2, 1);
	
	// iterate through with our test function
	linkedlist_iterate(list, traverse_example);

	// free the list
	linkedlist_destroy(&list);

	if (list == NULL) {
		printf("List deleted correctly\n");
	} else {
		printf("List not deleted!\n");
	}

	return 0;
}
