/*
	Teemu Vartiainen 2014
*/

#include <stdlib.h>
#include "ll_linkedlist.h"

int linkedlist_begin(ll_list **list) {

	// create new list
	ll_list *l = malloc(sizeof(ll_list));

	// initialize pointers to null
	l->first = NULL;
	l->last = NULL;

	// put the pointer in the location given
	*list = l;

	return 1;
}

int linkedlist_destroy(ll_list **list) {

	//if there's no list, nothing needs to be done
	if (*list == NULL) return 1;

	// get the pointer
	ll_list *l = *list;

	// if the list has nodes, free them
	if (l->first != NULL) {

		// initialize values
		ll_node *this = l->first;
		ll_node *next = this->next;

		// iterate through the next's until there's no next
		while (next != NULL) {
			next = this->next;
			// free the current node
			free(this);
			this = next;
		}

		// free the last node
		free(this);
	}

	// free the list
	free(l);

	// reset the pointer
	*list = NULL;

	return 1;
}


int linkedlist_append(ll_list **list, int x, int y, int connected) {

	// get the list pointer
	ll_list *l = *list;

	// allocate new node
	ll_node *new = malloc(sizeof(ll_node));

	// set the values
	new->xvalue = x;
	new->yvalue = y;
	new->connected = connected;
	new->next = NULL;

	// check if list is empty
	if (l->last == NULL) {
		l->first = new;
	} else {
		// set the last node's next to the new node
		l->last->next = new;
	}

	// update the last pointer
	l->last = new;

	return 1;
}


void linkedlist_iterate(ll_list *list, int (*iterfunc)(void *data) ) {

	// make sure we have something to iterate through
	if (list == NULL) return;
	if (list->first == NULL) return;

	// initialize with first
	ll_node *this = list->first;
	int a;

	// call iterfunc for every node
	do {
		a = iterfunc(this);
		if (a==0) return;
		this = this->next;
	} while (this != NULL);
}
