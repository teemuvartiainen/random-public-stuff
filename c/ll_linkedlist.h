/*
	Teemu Vartiainen 2014
*/

#ifndef LL_LINKEDLIST_H_
#define LL_LINKEDLIST_H_

// struct for the complete list
struct ll_list {
	struct ll_node *first;
	struct ll_node *last;
};

// struct for the list node, three ints
struct ll_node {
	int xvalue;
	int yvalue;
	int connected;
	struct ll_node *next;
};

typedef struct ll_list ll_list;
typedef struct ll_node ll_node;

/*
 * Start a new list at the pointer given
 *
 * returns: nonzero on success
 */
int linkedlist_begin(ll_list **list);

/*
 * Frees the list at the (root) pointer given
 *
 * returns: nonzero on success
 */
int linkedlist_destroy(ll_list **list);

/*
 * Appends a new item onto the list
 * with data x, y, connected
 *
 * returns: nonzero on success
 */

int linkedlist_append(ll_list **list, int x, int y, int connected);

/*
 * Calls given iterator function for every node in the list
 *
 * iterfunc needs to be a function like this:
 *  int iterator_function(void *n);
 *
 * it needs to return an int and take a pointer
 * it will be called with the pointer to the node,
 * so cast the void* into a ll_node* :)
 *
 * returns immediately if iterfunc returns zero
 */

void linkedlist_iterate(ll_list *list, int (*iterfunc)(void *data) );

#endif /* LL_LINKEDLIST_H_ */
